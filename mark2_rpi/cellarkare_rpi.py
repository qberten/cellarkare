#!/usr/bin/python
# coding=utf8
# CellarKare - Mark 2
# Un projet de station de mesure de la température et l'humidité dans une cave à vin
# Plus d'infos et schéma de montage sur http://wiki.makilab.org/index.php/CellarKare
# Quentin Berten, 2015 - Domaine public, licence WTFPL v2, http://www.wtfpl.net/
# quentin_#AT#_berten.me

import time

# Bibliothèque CharLCD d'Adafruit, voir https://github.com/adafruit/Adafruit_Python_CharLCD
# pour l'installation
import Adafruit_CharLCD as LCD
# Bibliothèque DHT d'AdaFruit, voir https://github.com/adafruit/Adafruit_Python_DHT
# pour l'installation
import Adafruit_DHT as DHT

# Configuration de l'affichage LCD
lcd_rs        = 25      # RS sur pin 25
lcd_en        = 24      # EN sur pin 24
lcd_d4        = 23      # D4 sur pin 23
lcd_d5        = 12      # D5 sur pin 12
lcd_d6        = 20      # D6 sur pin 20
lcd_d7        = 16      # D7 sur pin 16
lcd_backlight = 18      # rétro éclairage sur pin 18
lcd_columns   = 16      # Affichage à 16 colonnes ...
lcd_rows      = 2       # ... et 2 lignes
# Initialisation du LCD avec les variables ci-dessus
lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, 
		        	lcd_columns, lcd_rows, lcd_backlight)
# Configuration pour le capteur DHT, soit DHT11, DHT22 ou AM2302.
dht_sensor    = DHT.AM2302  # Type AM2302
dht_pin       = 5           # DHT data sur pin 5

# les variables qui permettent de gérer le temps,
prevMillis = 0          # enregistre le temps de la denière lecture DHT22
secInterval = 1000      # Un intervalle d'une seconde
# L'intervalle de lecture du DHT22: 3 secondes, doit etre plus grand que secInterval
dhtInterval = 3000

def read_and_print_dht():
    # On lit une valeur sur le senseur avec la fonction read. Attention, cette fonction
    # ne réessaie pas automatiquement en cas d'erreur, mais fournit les valeurs (None, None).
    hum, tempC  = DHT.read(dht_sensor, dht_pin)

    # On vérifie que l'on a bien eu une lecture (Linux n'est pas un OS temps réel et ne
    # peut pas garantir le timing des lectures sur le capteur). Si ça arrive, on réessaie.
    # On affiche les valeurs sur la console (n'est pas indispensable)
    if hum is not None and tempC is not None:
    	print 'Temp={0:0.2f}*C  Humidity={1:0.2f}%'.format(tempC, hum)
        # On affiche les valeurs sur l'écran LCD
        lcd.clear()
        lcd.message('deg C: {0:0.2f}\nhum %: {1:0.2f}'.format(tempC, hum))
    else:
	print 'Lecture sur le capteur DHT ratée'

# Boucle principale, l'équivalent de void loop() sur Arduino
while 1:
    # On lit le nombre de millisecondes courant
    curMillis = time.time()*1000
    if curMillis - prevMillis > dhtInterval:
        # On essaie de lire et afficher les valeurs du DHT
        read_and_print_dht()
        # on enregistre le dernier temps d'exécution de la lecture
        prevMillis = curMillis   
