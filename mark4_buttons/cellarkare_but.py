#!/usr/bin/python
# coding=utf8
# CellarKare - Mark 4
# Un projet de station de mesure de la température et l'humidité dans une cave à vin
# Plus d'infos et schéma de montage sur http://wiki.makilab.org/index.php/CellarKare
# Quentin Berten, 2015 - Domaine public, licence WTFPL v2, http://www.wtfpl.net/
# quentin_#AT#_berten.me

# Une série de module python de la bibliothèque standard
import datetime, time, subprocess, logging, sys, signal, socket
# Bibliothèque Rpi.GPIO pour la gestion des pin IN/OUT
import RPi.GPIO as GPIO
# Bibliothèque CharLCD d'Adafruit, voir https://github.com/adafruit/Adafruit_Python_CharLCD
# pour l'installation
import Adafruit_CharLCD as LCD
# Bibliothèque DHT d'AdaFruit, voir https://github.com/adafruit/Adafruit_Python_DHT
# pour l'installation
import Adafruit_DHT as DHT
# Bibliothèque picamera voir https://www.raspberrypi.org/learning/python-picamera-setup/worksheet.md
import picamera as PICAM

# On gère le niveau de log voulu (voir par exemple https://docs.python.org/2.7/howto/logging.html)
# A modifier une fois le débogage terminé ;-)
logging.basicConfig(filename='/tmp/cellarkare.log', level=logging.DEBUG)

# Constantes de configuration de l'affichage LCD
LCD_RS        = 10      # RS
LCD_EN        = 22      # EN
LCD_D4        = 27      # D4
LCD_D5        = 17      # D5
LCD_D6        = 4      # D6
LCD_D7        = 3       # D7
LCD_BACKLIGHT = 2       # rétro éclairage (via PWM)
LCD_COLUMNS   = 16      # Affichage à 16 colonnes ...
LCD_ROWS      = 2       # ... et 2 lignes
# Une variable contenant la valeur de d'éclairage du fond de l'affichage pour la modulation PWM
lcd_backlight = 0

# Configuration pour le capteur DHT, soit DHT11, DHT22 ou AM2302.
DHT_SENSOR    = DHT.AM2302  # Type AM2302
DHT_PIN       = 15          # DHT data sur pin 15
# La pin pour le bouton OFF
OFF_PIN       = 21          # Bouton sur pin 21
# La pin pour le bouton ENTER (camera)
ENTER_PIN     = 14
# La pin pour les boutons MINUS et PLUS
MINUS_PIN     = 12
PLUS_PIN      = 24 
# les pins pour le chargement/déchargement du circuit RC de la photorésistance
LIGHT_PIN_CHA = 7
LIGHT_PIN_DIS = 8
# La pin pour la led POWER/OFF
OFF_LED       = 20

# les variables qui permettent de gérer le temps
cur_millis = time.time()*1000
dht_millis = 0          # enregistre le temps de la denière lecture DHT22
off_millis = 0           # enregistre le temps du dernier appui sur le bouton OFF
led_millis = 0           # enregistre le temps du dernier blink des led(s) de statut
SEC_INTERVAL = 1000      # Un intervalle d'une seconde
# L'intervalle de lecture du DHT22: 3 secondes, doit etre plus grand que SEC_INTERVAL
DHT_INTERVAL = 5000
# L'intervalle de temps (en ms) pour considérer une pressions sur le bouton OFF comme un reset
OFF_INTERVAL = 3000
# L'intervalle de temps (en ms) de déchargemenent de la capacité de mesure de la photo
# résistance qui va déclencher un changement d'état de l'écran
LIGHT_CAPA_INTERVAL = 2500

# Pour la led d'état:
# vert plein: powered on.
# bleue: pulsant lent: mesures OK, pulsant court: plus de 10 mesures DHT ratées.

# Une variable d'état pour le bouton OFF: 1:DOWN, 0:UP
off_state     = False
# Une variable d'état pour les led(s) de status OFF: 1:ON 0: OFF
led_state     = True
# Une variable d'état contenant l'adresse IP. Contient "" si pas connecté au réseau
net_ip        = ""

# La fonction qui effectue l'arrêt propre du RPi
def btn_off(pin):
    global off_state, cur_millis, off_millis, lcd, net_ip
    # On lit le nombre de millisecondes courant
    cur_millis = time.time()*1000
    # Utilise la variable globale off_state
    off_state = not off_state
    # DEBUG
    logging.debug("Bouton OFF_PIN pressé, son état est maintenant %s" % str(off_state))
    if off_state == True:
        off_millis = cur_millis   
    elif off_state == False: 
        # DEBUG
        logging.debug("Bouton OFF_PIN pressé pendant {0:0.0f} ms".format(cur_millis - off_millis))
        if (cur_millis - off_millis) > OFF_INTERVAL:
            # On a relâché le bouton avec un écart long => shutdown
            lcd.clear()
            lcd.message("Arret. 10 s\navant debrancher")
            logging.debug("Appui long sur le bouton OFF_PIN")
            logging.info("Arrêt du Raspberry PI, attendre encore 10 sec avant de débrancher")
            subprocess.call('halt', shell=False)
        else:
            # Appuis court => on ne fait rien
            logging.debug("Appui court sur le bouton OFF_PIN")
            # On affiche les valeurs sur l'écran LCD
            lcd.clear()
            lcd.message("Min 3s pour OFF\n" + net_ip)

def btn_enter(pin):
    # DEBUG
    logging.info("Bouton ENTER_PIN pressé, on prend un photo")
    # Construction du chemin pour la photo
    now = datetime.datetime.now()
    pic_filename = "/home/qb/pic_" + str(now) + ".jpg"
    # Initialisation de la camera. On ne le fait pas dans initialize() pour éviter au maximum de la laisser
    # allumée (et consommer) en continu. En plus, une fois activée, les lectures sur le DHT sont beaucoup
    # plus souvent ratées.
    with PICAM.PiCamera() as cam:
        cam.capture(pic_filename)
        logging.debug("Photos sauvée " + str(cam.resolution) + "  dans " + pic_filename)

def btn_minus(pin):
    # DEBUG
    logging.info("Bouton MINUS_PIN pressé")

def btn_plus(pin):
    # DEBUG
    logging.info("Bouton PLUS_PIN pressé")

# La fonction de lecture/affichage des valeurs de T° et Humidité
def read_and_print_dht():
    global lcd
    # On lit une valeur sur le senseur avec la fonction read. Attention, cette fonction
    # ne réessaie pas automatiquement en cas d'erreur, mais fournit les valeurs (None, None).
    hum, tempC  = DHT.read(DHT_SENSOR, DHT_PIN)

    # On vérifie que l'on a bien eu une lecture (Linux n'est pas un OS temps réel et ne
    # peut pas garantir le timing des lectures sur le capteur). Si ça arrive, on réessaie.
    # On affiche les valeurs sur la console (n'est pas indispensable)
    if hum is not None and tempC is not None:
    	logging.info("Temp={0:0.2f}*C  Humidity={1:0.2f}%".format(tempC, hum))
        # On affiche les valeurs sur l'écran LCD
        lcd.clear()
        lcd.message("deg C: {0:0.2f}\nhum %: {1:0.2f}".format(tempC, hum))
    else:
	logging.info("Lecture sur le capteur DHT ratée")

def light_rc_discharge():
    # DEBUG
    logging.debug("Déchargement du circuit RC de la photorésistance")
    GPIO.setup(LIGHT_PIN_CHA, GPIO.IN)
    GPIO.setup(LIGHT_PIN_DIS, GPIO.OUT)
    GPIO.output(LIGHT_PIN_DIS, False)
    time.sleep(1.5)

def light_rc_charge():
    # DEBUG
    logging.debug("Chargement du circuit RC de la photorésistance")
    GPIO.setup(LIGHT_PIN_DIS, GPIO.IN)
    GPIO.setup(LIGHT_PIN_CHA, GPIO.OUT)
    logging.debug("Setting the output in light_rc_charge()")
    GPIO.output(LIGHT_PIN_CHA, True)
    # On lit le nombre de millisecondes courant
    light_init_millis = time.time()*1000
    light_delay_millis = (time.time()*1000) - light_init_millis
    while not (GPIO.input(LIGHT_PIN_DIS) or (light_delay_millis > LIGHT_CAPA_INTERVAL)):
        light_delay_millis = (time.time()*1000) - light_init_millis
        # logging.debug("Count of Photoresistance= " + str(light_delay_millis))
    return light_delay_millis

def lcd_dim_backlight_on():
    global lcd
    global lcd_backlight
    if lcd_backlight == 1:
        return
    else:
        for x in range(0,11,1):
            lcd.set_backlight(x*0.1)
            time.sleep(0.05)
        lcd_backlight = 1

def lcd_dim_backlight_off():
    global lcd
    global lcd_backlight
    if lcd_backlight == 0:
        return
    else:
        for x in range(10,-1,-1):
            lcd.set_backlight(x*0.1)
            time.sleep(0.05)
        lcd_backlight = 0
    lcd.clear()

def read_light_rc_and_ajust_lcd_backlight():
    # DEBUG
    logging.debug("Lecture valeur photorésistance via circuit RC")
    light_rc_discharge()
    light_value = light_rc_charge()
    logging.debug("Final count of Photoresistance= " + str(light_value))
    if light_value < LIGHT_CAPA_INTERVAL:
        logging.debug("High light value, turning lcd on")
        lcd_dim_backlight_on()
    else:
        logging.debug("Low light value, turning lcd off")
        lcd_dim_backlight_off()

def check_ip():
    try:
        # voir http://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
        return [(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]
    except:
        return ""


# Fonction d'initialisation
def initialize():
    global lcd, led_state, net_ip
    logging.debug("Initialize function started")
    # Initialisation du LCD avec les variables ci-dessus
    lcd = LCD.Adafruit_CharLCD(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 
		        	LCD_COLUMNS, LCD_ROWS,
                                LCD_BACKLIGHT, invert_polarity = False, enable_pwm = True,
                                initial_backlight = lcd_backlight)
    # Initialisation des autres GPIO's
    GPIO.setmode(GPIO.BCM)        # On utilise la numérotation BCM pour les PIN
    # On définit la pin OFF_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(OFF_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter les mouvements up et down sur le bouton
    GPIO.add_event_detect(OFF_PIN, GPIO.BOTH, callback=btn_off, bouncetime=20)
    # On définit la pin ENTER_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(ENTER_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter la remontée "up" sur le bouton
    GPIO.add_event_detect(ENTER_PIN, GPIO.RISING, callback=btn_enter, bouncetime=200)
    # On définit la pin MINUS_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(MINUS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter la remontée "up" sur le bouton
    GPIO.add_event_detect(MINUS_PIN, GPIO.RISING, callback=btn_minus, bouncetime=200)
    # On définit la pin PLUS_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(PLUS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter la remontée "up" sur le bouton
    GPIO.add_event_detect(PLUS_PIN, GPIO.RISING, callback=btn_plus, bouncetime=200)
    # Configure la pin pour la led verte de statut
    GPIO.setup(OFF_LED, GPIO.OUT)
    GPIO.output(OFF_LED, led_state)
    net_ip = check_ip()
    # Affichage d'un message de bienvenue sur l'écran, et allumage "smooth" de l'écran
    lcd.clear()
    lcd.message("CellarKare - Mk4\nWelcome")
    lcd_dim_backlight_on()
    logging.debug("Initialize function ended")

# Fonction de terminaison du programme (appelée lorsque le script est arrêté
def terminate():
    logging.debug("Terminate function started")
    # efface l'écran LCD
    lcd_dim_backlight_off()
    lcd.clear()
    # Cleanup des entrées/sorties
    GPIO.cleanup()
    logging.debug("Terminate function ended")

# Fonction de la boucle principale (léquivalent de void loop() sur Arduino)
def loop():
    global cur_millis, dht_millis, led_millis, led_state, net_ip
    # On lit le nombre de millisecondes courant
    cur_millis = time.time()*1000
    # DEBUG
    if (cur_millis - led_millis > SEC_INTERVAL) and not net_ip:
        # On éteint / blink la LED verte si pas de réseau (ip nulle)
        led_state = not led_state
        logging.debug("OFF_LED state is " + str(led_state))
        GPIO.output(OFF_LED, led_state)
        led_millis = cur_millis
    if cur_millis - dht_millis > DHT_INTERVAL:
        # On essaie de lire et afficher les valeurs du DHT
        read_and_print_dht()
        # On lit la valeur de la photorésistance
        read_light_rc_and_ajust_lcd_backlight()
        # On vérifie la connection au réseau et on met à jour l'IP
        net_ip = check_ip()
        logging.debug("IP adress updated, is now: " + net_ip)
        # on enregistre le dernier temps d'exécution de la lecture
        dht_millis = cur_millis   

# On s'assure que lorsqu'on termine le script avec un kill, le script se termine proprement
def signal_handler(signal, frame):
    sys.exit(0)
signal.signal(signal.SIGTERM, signal_handler)

# Actionnement de l'initialisation et de la boucle principale 
initialize()
try:
    while 1:
        loop()
# On récupère les exceptions de terminaison, et on exécute la fonction terminate
except (KeyboardInterrupt, SystemExit) as e:
    terminate()
    sys.exit(e)
