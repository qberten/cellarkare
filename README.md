L'ensemble des fichiers sources pour le projet "CellarKare".
Pour plus d'informations: [le projet documenté sur le wiki du Makilab](http://wiki.makilab.org)

The set of source files for the "CellarKare" project.
For more information: [the project documentation on the Makilab wiki](http://wiki.makilab.org)
