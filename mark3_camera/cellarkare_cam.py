#!/usr/bin/python
# coding=utf8
# CellarKare - Mark 3
# Un projet de station de mesure de la température et l'humidité dans une cave à vin
# Plus d'infos et schéma de montage sur http://wiki.makilab.org/index.php/CellarKare
# Quentin Berten, 2015 - Domaine public, licence WTFPL v2, http://www.wtfpl.net/
# quentin_#AT#_berten.me

# Une série de module python de la bibliothèque standard
import datetime, time, subprocess, logging, sys, signal
# Bibliothèque Rpi.GPIO pour la gestion des pin IN/OUT
import RPi.GPIO as GPIO
# Bibliothèque CharLCD d'Adafruit, voir https://github.com/adafruit/Adafruit_Python_CharLCD
# pour l'installation
import Adafruit_CharLCD as LCD
# Bibliothèque DHT d'AdaFruit, voir https://github.com/adafruit/Adafruit_Python_DHT
# pour l'installation
import Adafruit_DHT as DHT
# Bibliothèque picamera voir https://www.raspberrypi.org/learning/python-picamera-setup/worksheet.md
import picamera as PICAM

# On gère le niveau de log voulu (voir par exemple https://docs.python.org/2.7/howto/logging.html)
# A modifier une fois le débogage terminé ;-)
logging.basicConfig(filename='/tmp/cellarkare.log', level=logging.DEBUG)

# Constantes de configuration de l'affichage LCD
LCD_RS        = 25      # RS sur pin 25
LCD_EN        = 24      # EN sur pin 24
LCD_D4        = 23      # D4 sur pin 23
LCD_D5        = 12      # D5 sur pin 12
LCD_D6        = 20      # D6 sur pin 20
LCD_D7        = 16      # D7 sur pin 16
LCD_BACKLIGHT = 18      # rétro éclairage sur pin 18
LCD_COLUMNS   = 16      # Affichage à 16 colonnes ...
LCD_ROWS      = 2       # ... et 2 lignes
# Une variable contenant la valeur de d'éclairage du fond de l'affichage pour la modulation PWM
lcd_backlight = 0.0

# Configuration pour le capteur DHT, soit DHT11, DHT22 ou AM2302.
DHT_SENSOR    = DHT.AM2302  # Type AM2302
DHT_PIN       = 5           # DHT data sur pin 5
# La pin pour le bouton OFF
OFF_PIN       = 4           # Bouton sur pin 4
# Une variable d'état pour le bouton OFF: 1:DOWN, 0:UP
off_state     = False
# La pin pour le bouton CAM (camera)
CAM_PIN       = 17

# les variables qui permettent de gérer le temps
cur_millis = time.time()*1000
prev_millis = 0          # enregistre le temps de la denière lecture DHT22
off_millis = 0           # enregistre le temps du dernier appui sur le bouton OFF
SEC_INTERVAL = 1000      # Un intervalle d'une seconde
# L'intervalle de lecture du DHT22: 3 secondes, doit etre plus grand que SEC_INTERVAL
DHT_INTERVAL = 3000
# L'intervalle de temps (en ms) pour considérer une pressions sur le bouton OFF comme un reset
OFF_INTERVAL = 3000

# La fonction qui effectuer l'arrêt propre du RPi
def btn_proper_halt(pin):
    global off_state, cur_millis, off_millis
    # On lit le nombre de millisecondes courant
    cur_millis = time.time()*1000
    # Utilise la variable globale off_state
    off_state = not off_state
    # DEBUG
    logging.debug("Bouton OFF_PIN pressé, son état est maintenant %s" % str(off_state))
    if off_state == True:
        off_millis = cur_millis   
    elif off_state == False: 
        # DEBUG
        logging.debug("Bouton OFF_PIN pressé pendant {0:0.0f} ms".format(cur_millis - off_millis))
        if (cur_millis - off_millis) > OFF_INTERVAL:
            # On a relâché le bouton avec un écart long => shutdown
            logging.debug("Appui long sur le bouton OFF_PIN")
            logging.info("Arrêt du Raspberry PI, attendre encore 10 sec avant de débrancher")
            subprocess.call('halt', shell=False)
        else:
            # Appuis court => on ne fait rien
            logging.debug("Appui court sur le bouton OFF_PIN")

# La fonction de lecture/affichage des valeurs de T° et Humidité
def read_and_print_dht():
    global lcd
    # On lit une valeur sur le senseur avec la fonction read. Attention, cette fonction
    # ne réessaie pas automatiquement en cas d'erreur, mais fournit les valeurs (None, None).
    hum, tempC  = DHT.read(DHT_SENSOR, DHT_PIN)

    # On vérifie que l'on a bien eu une lecture (Linux n'est pas un OS temps réel et ne
    # peut pas garantir le timing des lectures sur le capteur). Si ça arrive, on réessaie.
    # On affiche les valeurs sur la console (n'est pas indispensable)
    if hum is not None and tempC is not None:
    	logging.info("Temp={0:0.2f}*C  Humidity={1:0.2f}%".format(tempC, hum))
        # On affiche les valeurs sur l'écran LCD
        lcd.clear()
        lcd.message("deg C: {0:0.2f}\nhum %: {1:0.2f}".format(tempC, hum))
    else:
	logging.info("Lecture sur le capteur DHT ratée")

def btn_take_picture(pin):
    # DEBUG
    logging.info("Bouton CAM_PIN pressé, on prend un photo")
    # Construction du chemin pour la photo
    now = datetime.datetime.now()
    pic_filename = "/home/qb/pic_" + str(now) + ".jpg"
    # Initialisation de la camera. On ne le fait pas dans initialize() pour éviter au maximum de la laisser
    # allumée (et consommer) en continu. En plus, une fois activée, les lectures sur le DHT sont beaucoup
    # plus souvent ratées.
    with PICAM.PiCamera() as cam:
        cam.capture(pic_filename)
        logging.debug("Photos sauvée " + str(cam.resolution) + "  dans " + pic_filename)

# Module l'intensité du rétro éclairage de l'écran lcd
def lcd_dim_backlight(backlight_value, time_step):
    global lcd
    global lcd_backlight
    # calcule l'incrément à avoir entre lcd_backlight et backlight_value
    stp = (lcd_backlight > backlight_value) * -2 +1
    brightnesses = map(lambda x: x/10.0, range(int(lcd_backlight*10), int(backlight_value*10)+1, stp))
    for bright in brightnesses:
        lcd_backlight = bright
        logging.debug("loop on bright= " + str(bright))
        lcd.set_backlight(bright)
        time.sleep(time_step)


# Fonction d'initialisation
def initialize():
    global lcd
    logging.debug("Initialize function started")
    # Initialisation du LCD avec les variables ci-dessus
    lcd = LCD.Adafruit_CharLCD(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7, 
		        	LCD_COLUMNS, LCD_ROWS,
                                LCD_BACKLIGHT, invert_polarity = False, enable_pwm = True,
                                initial_backlight = lcd_backlight)
    # Initialisation des autres GPIO's
    GPIO.setmode(GPIO.BCM)        # On utilise la numérotation BCM pour les PIN
    # On définit la pin OFF_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(OFF_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter les mouvements up et down sur le bouton
    GPIO.add_event_detect(OFF_PIN, GPIO.BOTH, callback=btn_proper_halt, bouncetime=20)
    # On définit la pin CAM_PIN comme entrée, et on active la résistance pull-down interne du RPi
    GPIO.setup(CAM_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # Configure une interruption qui va détecter la remontée "up" sur le bouton
    GPIO.add_event_detect(CAM_PIN, GPIO.RISING, callback=btn_take_picture, bouncetime=200)
    # Affichage d'un message de bienvenue sur l'écran, et allumage "smooth" de l'écran
    lcd.clear()
    lcd.message("CellarKare - Mk3\nWelcome")
    lcd_dim_backlight(1.0, 0.2)
    logging.debug("Initialize function ended")

# Fonction de terminaison du programme (appelée lorsque le script est arrêté
def terminate():
    logging.debug("Terminate function started")
    # efface l'écran LCD
    lcd_dim_backlight(0.0, 0.2)
    lcd.clear()
    logging.debug("Terminate function ended")

# Fonction de la boucle principale (léquivalent de void loop() sur Arduino)
def loop():
    global cur_millis, prev_millis
    # On lit le nombre de millisecondes courant
    cur_millis = time.time()*1000
    # DEBUG
    if cur_millis - prev_millis > DHT_INTERVAL:
        # On essaie de lire et afficher les valeurs du DHT
        read_and_print_dht()
        # on enregistre le dernier temps d'exécution de la lecture
        prev_millis = cur_millis   

# Actionnement de l'initialisation et de la boucle principale 
def signal_handler(signal, frame):
    sys.exit(0)

# On s'assure que lorsqu'on termine le script avec un kill, le script se termine proprement
signal.signal(signal.SIGTERM, signal_handler)

initialize()
try:
    while 1:
        loop()
# On récupère les exceptions de terminaison, et on exécute la fonction terminate
except (KeyboardInterrupt, SystemExit) as e:
    terminate()
    sys.exit(e)
