/*
CellarKare - Mark #1
Un projet de station de mesure de la température et l'humidité dans une cave à vin
Plus d'infos et schéma de montage sur http://wiki.makilab.org/index.php/CellarKare
Quentin Berten, 2015 - Domaine public, licence WTFPL v2, http://www.wtfpl.net/
quentin_#AT#_berten.me
*/

// Bibliothèque LiquidCrystal, pour l'écran LCD. Installée de base avec l'IDE
#include <LiquidCrystal.h>

// Bibliothèque DHT, pour le capteur de température. Installable directement depuis
// l'IDE, via Sketch -> Include LIbrary -> Manage Libraries -> DHT sensor Library 
#include "DHT.h"

// Définition l'écran LCD, en argument les pins d'interface, dans l'ordre
// (RS pin, LCD Enable, D4, D5, D6, D7)
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Définition des pin et type pour le senseur de t° et humidité
#define DHTPIN 7         // senseur (fil data, en jaune), sur pin 7
#define DHTTYPE DHT22    // type DHT 22  (AM2302)

// Définition du capteur DHT22, pour un Arduino "normal" à 16mhz, comme le Uno
DHT dht(DHTPIN, DHTTYPE);
// les variables associées qui vont lire les valeurs (température, ...)
float hum;
float tempC;
float tempF;
float indiceHum;

// les variables qui permettent de gérer le temps,
// voir http://www.arduino.cc/en/pmwiki.php?n=Tutorial/BlinkWithoutDelay
long prevMillis = 0;           // enregistre le temps de la denière lecture DHT22
long secInterval = 1000;       // Un intervalle d'un seconde
// L'intervalle de lecture du DHT22: 3secondes, doit etre plus grand que secInterval
long dhtInterval = 3000;

// la procédure d'initialisation arduino
void setup() {
voif djfqslk

  // on ouvre une connection série (pour afficher un maxium d'info)
  Serial.begin(9600); 
  Serial.println("CellarKare Mark #1 Test");

  // initialisation du capteur
  dht.begin();
  // initialisation de l'écran, 16 colonnes et 2 lignes
  lcd.begin(16, 2);
}

// la boucle principale arduino
void loop() {
  unsigned long curMillis = millis();
  
  if(curMillis - prevMillis < dhtInterval) {
    goto end_loop;        // pas de lecture DHT, on va à la fin de la boucle loop()
  }
  
  // on enregistre le dernier temps d'exécution de la lecture
  prevMillis = curMillis;   
  
  // la lecture de la t° et l'humidité prends environ 250 millisecondes
  // mais les lectures du capteur peuvent etre vieillies de 2 secondes
  // le capteur n'est pas rapide (d'où l'attente de 3 secondes pour etre
  // certain d'avoir des lectures correctes.
  // lecture de l'humidité
  hum = dht.readHumidity();
  // lecture de la température en degrés Celsius
  tempC = dht.readTemperature();
  // lecture de la température en degrés Fahreneit
  tempF = dht.readTemperature(true);
  
  // On vérifie que les lectures on bien été réalisées. Si pas, on quitte
  // la boucle pour ré-essayer
  if (isnan(hum) || isnan(tempC) || isnan(tempF)) {
    Serial.println("Lecture sur le capteur DHT ratée");
    goto end_loop;
  }

  // Calcul de l'indice de chaleur (voir https://fr.wikipedia.org/wiki/Indice_de_chaleur )
  // La température doit etre fournie en degré Fahreneit
  indiceHum = dht.computeHeatIndex(tempF, hum);

  // On affiche les valeurs sur la console série (n'est pas indispensable)
  Serial.print("Humidite: "); 
  Serial.print(hum);
  Serial.print(" %\t");
  Serial.print("Temperature: "); 
  Serial.print(tempC);
  Serial.print(" *C ");
  Serial.print(tempF);
  Serial.print(" *F\t");
  Serial.print("Indice de chaleur: ");
  Serial.print(indiceHum);
  Serial.println(" *F");
  
  // On affiche les valeurs sur l'écran LCD, curseur à (0,0)
  lcd.setCursor(0, 0);
  // print Temp in C°
  lcd.print("deg C: ");
  lcd.print(tempC);
  lcd.setCursor(0,1);
  lcd.print("hum %: ");
  lcd.print(hum);

  // label pour goto
  end_loop:
  ;
}
